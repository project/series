<?php
// $Id$

/**
 * Implementation of hook_data().
 */
function series_views_data() {
  $data['series_weight']['table']['group'] = t('Series');
  $data['series_weight']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid'
  );
  $data['series_weight']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The weight of a node in a series.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'series_handler_filter_series_node_weight'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  $data['series_user_progress']['table']['group'] = t('Series');
  $data['series_user_progress']['table']['join']['node'] = array(
    'left_table' => 'term_node',
    'left_field' => 'nid',
    'field' => 'id',
    'extra' => array(
      array('field' => 'field', 'value' => 'nid'),
      array('field' => 'uid', 'value' => '***CURRENT_USER***'),
    ),
  );
  $data['series_user_progress']['access'] = array(
    'real field' => 'completed',
    'title' => t('Access'),
    'help' => t('Filter by access.'),
    'filter' => array(
      'handler' => 'series_handler_filter_node_access',
    ),
  );

  $data['term_data']['series_vocabulary'] = array(
    'real field' => 'vid',
    'title' => t('Series vocabulary'),
    'help' => t('The Series vocabulary.'),
    'group' => t('Series'),
    'filter' => array(
      'handler' => 'series_handler_filter_series_vocabulary'
    )
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function series_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'series') .'/views/handlers'
    ),
    'handlers' => array(
      'series_handler_filter_series_node_weight' => array(
        'parent' => 'views_handler_filter',
      ),
      'series_handler_filter_series_vocabulary' => array(
        'parent' => 'views_handler_filter',
      ),
      'series_handler_filter_node_access' => array(
        'parent' => 'views_handler_filter',
      ),
    )
  );
}

/**
 * Implementation of hook_views_plugins
 */
function series_views_plugins() {
  return array(
    'argument validator' => array(
      'series_id' => array(
        'title' => t('Series ID'),
        'handler' => 'series_plugin_argument_validate_series_id',
        'path' => drupal_get_path('module', 'series') .'/views/plugins',
        'theme path' => drupal_get_path('module', 'series') .'/views/theme'
      ),
    )
  );
}
