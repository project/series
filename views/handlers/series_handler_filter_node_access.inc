<?php

class series_handler_filter_node_access extends views_handler_filter {
  function admin_summary() { }

  function query() {
    $table = $this->ensure_my_table();
    $node_table = $this->view->query->tables['node']['node']['alias'];
    $term_node_table = $this->view->query->tables['node']['term_node']['alias'];

    $first_sql = "SELECT sw.nid FROM {series_weight} sw WHERE sw.tid = $term_node_table.tid ORDER BY sw.weight ASC LIMIT 1";
    $prev_complete_sql = "SELECT sp.completed FROM {series_user_progress} sp WHERE sp.field = 'nid' AND sp.id = (SELECT sw.nid FROM {series_weight} sw WHERE sw.tid = $term_node_table.tid AND sw.weight < (SELECT sw2.weight FROM {series_weight} sw2 WHERE sw2.tid = $term_node_table.tid AND sw2.nid = $node_table.nid ORDER BY sw2.weight ASC LIMIT 1) ORDER BY sw.weight DESC LIMIT 1) AND sp.uid = ***CURRENT_USER***";

    $this->query->add_where($this->options['group'], "(SELECT sc.control_type FROM {series_config} sc WHERE sc.tid = $term_node_table.tid) = " . SERIES_CONTROL_OPEN . " OR ($table.completed <> 0 OR $node_table.nid = ($first_sql) OR ($prev_complete_sql) <> 0)");
  }
}
