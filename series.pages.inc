<?php

function series_conclusion_page($series) {
  if (empty($series->conclusion['body'])) {
    // If no conclusion, show a page not found.
    drupal_not_found();
    return;
  }

  $content = array(
    'body' => array(
      '#type' => 'markup',
      '#value' => check_markup($series->conclusion['body'], $series->conclusion['format']),
      '#weight' => -10,
    ),
    'series' => series_content($series),
  );
  return drupal_render($content);
}
