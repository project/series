// $Id: admin.js,v 1.1 2010/07/27 15:20:50 greenskin Exp $

Drupal.behaviors.seriesAdmin = function() {
  // Auto-submit when selecting a type of content to add.
  $('#edit-add-field-select').change(function() {
    if ($(this).val() != 0) {
      $('#edit-add-field-add').click();
    }
  });

  // Toggle visibility of time control settings.
  var $timeSettingsFieldset = $('fieldset.time-settings');
  $('#edit-control-type').change(function() {
    if ($(this).val() == 2) {
      $timeSettingsFieldset.removeClass('js-hide');
    }
    else {
      $timeSettingsFieldset.addClass('js-hide');
    }
  });
}
