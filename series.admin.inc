<?php

/**
 * @file
 * Admin page functions.
 *
 * @author greenSkin
 */

function series_admin_form() {
  $options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }

  $form['series_vid'] = array(
    '#type' => 'select',
    '#title' => t('Series vocabulary'),
    '#description' => t('The taxonomy vocabulary that will be considered the series.'),
    '#default_value' => variable_get('series_vid', 0),
    '#options' => $options,
    '#required' => TRUE
  );

  return system_settings_form($form);
}

function series_add_page() {
  if ($vocabulary = taxonomy_vocabulary_load(variable_get('series_vid', 0))) {
    return drupal_get_form('series_add_form', $vocabulary);
  }

  return t('A vocabulary has not yet been set as the series vocabulary. Please set the !link.', array('!link' => l(t('Series vocabulary'), 'admin/settings/series')));
}

function series_add_form($form, &$form_state, $vocabulary) {
  $form_state['vocabulary'] = &$vocabulary;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 255,
    '#description' => t('The name of this series.'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A description of the series. This will appear on the series overview page.'),
  );
  $form['vid'] = array(
    '#type' => 'value',
    '#value' => $vocabulary->vid,
  );

  $types = series_get_vocabulary_node_types($vocabulary);
  if (!empty($types)) {
    $options = array();
    foreach (_node_types_build()->types as $type) {
      if (isset($types[$type->type]) && user_access('create ' . $type->type . ' content')) {
        $options[$type->type] = $type->name;
      }
    }
    $form['node_type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => $options,
    );
  }

  if (module_exists('taxonomy_image')) {
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
    $form['taxonomy_image'] = array(
      '#type' => 'fieldset',
      '#title' => t('Image'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['taxonomy_image']['new_image']['taxonomy_image_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload image'),
      '#size' => 60,
      '#description' => t('The image file you wish to associate to this series.'),
    );

    $form['taxonomy_image']['taxonomy_image_external'] = array(
      '#type' => 'textfield',
      '#title' => t('Use external image'),
      '#cols' => 60,
      '#description' => t('Enter a path to an external image. Note this image will be copied to this site.'),
      '#prefix' => '<strong>' . t('or:') . '</strong>',
    );

    $form['#validate'] = array('taxonomy_image_validate_external');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function series_add_form_submit($form, &$form_state) {
  module_load_include('inc', 'taxonomy', 'taxonomy.admin');
  $dummy_state = array();
  $dummy_state['values']['name'] = $form_state['values']['name'];
  $dummy_state['values']['description']['value'] = $form_state['values']['description'];
  $dummy_state['values']['vid'] = $form_state['values']['vid'];
  $dummy_state['values']['op'] = t('Save');

  if (isset($form['taxonomy_image'])) {
    $dummy_state['values']['taxonomy_image_upload'] = $form_state['values']['taxonomy_image_upload'];
    $dummy_state['values']['taxonomy_image_external'] = $form_state['values']['taxonomy_image_external'];
  }

  drupal_form_submit('taxonomy_form_term', $dummy_state, $form_state['vocabulary']);

  if (!empty($form_state['values']['node_type'])) {
    $form_state['redirect'] = array(
      'node/add/' . $form_state['values']['node_type'],
      array('series' => $dummy_state['values']['vid']) + drupal_get_destination(),
    );
  }
  else if (isset($dummy_state['redirect'])) {
    $form_state['redirect'] = $dummy_state['redirect'];
  }
}

function series_node_edit_form($form, $form_state, $node) {
  $series = series_load($node);
  return series_edit_form($form, $form_state, $series);
}

function series_edit_form($form, $form_state, $series) {
  $form = array(
    '#theme' => 'series_edit_form',
    '#tree' => TRUE,
    '#series' => $series,
    'nodes' => array()
  );

  $form['vid'] = array(
    '#type' => 'value',
    '#value' => $series->vid
  );
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => $series->tid
  );

  foreach ($series->nodes as $nid => $series_node) {
    $form['nodes'][$nid]['nid'] = array(
      '#type' => 'hidden',
      '#value' => $nid
    );
    $form['nodes'][$nid]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#default_value' => $series_node['weight']
    );
  }

  $form += series_create_form($form, $form_state, $series);

  $form['information'] = array(
    '#type' => 'fieldset',
    '#title' => t('Information'),
    '#description' => l(t('Edit vocabulary'), 'admin/content/taxonomy/edit/vocabulary/' . $series->vid),
    '#collapsible' => TRUE,
    '#collapsed' => ($series->description) ? TRUE : FALSE
  );
  $title_value = l($series->name, 'series/'. $series->tid);
  if (user_access('administer taxonomy')) $title_value .= ' ('. l('edit', 'admin/content/taxonomy/edit/term/'. $series->tid, array('query' => drupal_get_destination())) .')';
  $form['information']['title'] = array(
    '#type' => 'item',
    '#title' => 'Series Title',
    '#value' => $title_value
  );
  $form['information']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $series->description,
    '#description' => t('A description of the series. This will appear on the series overview page.')
  );

  $form['control'] = array(
    '#type' => 'fieldset',
    '#title' => t('Control system'),
    '#description' => t('The control system determines how a user can progress through a series. There are three types to choose from: Open, Consecutive, and Time. Open allows the user to jump around within the series in any order they wish. Consecutive requires the user to progress from start to finish, in the order the series is laid out in. Time is identical to Consecutive with the exception that progress is slowed by a configurable speed.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['control']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#description' => t('Specify what type of control to use.'),
    '#default_value' => $series->control['type'],
    '#options' => array(SERIES_CONTROL_OPEN => t('Open'), SERIES_CONTROL_CONSECUTIVE => t('Consecutive'), SERIES_CONTROL_CONSECUTIVE_TIME => t('Consecutive-time')),
    '#required' => TRUE,
    '#weight' => -1,
  );
  $form['control']['time'] = array(
    '#type' => 'fieldset',
    '#title' => t('Time settings'),
    '#description' => t("When the control type is set to <em>Time</em>, a user's speed through a series is restricted. The time restriction is relative to when the user first starts the series. For example, a user cannot access the second item in a series until one set of the specified steps of unit have passed."),
    '#collapsible' => FALSE,
    '#attributes' => array('class' => array('time-settings')),
  );
  if ($series->control['type'] != SERIES_CONTROL_CONSECUTIVE_TIME) {
    $form['control']['time']['#attributes']['class'][] = 'js-hide';
  }
  $form['control']['time']['unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit of time'),
    '#default_value' => $series->control['time']['unit'],
    '#options' => array('day' => t('Day'), 'week' => t('Week')),
  );
  $form['control']['time']['step'] = array(
    '#type' => 'select',
    '#title' => t('Step'),
    '#default_value' => $series->control['time']['step'],
    '#options' => drupal_map_assoc(range(1, 52)),
  );

  // $form['conclusion']['body'] = array(
  //   '#type' => 'textarea',
  //   '#title' => t('Conclusion'),
  //   '#description' => t('The conclusion will be presented to the user at the completion of the series.'),
  //   '#default_value' => $series->conclusion['body'],
  // );
  // $form['conclusion']['format'] = filter_form($series->conclusion['format'], NULL, array('conclusion', 'format'));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('series_edit_form_submit')
  );

  return $form;
}

function series_edit_form_submit($form, &$form_state) {
  db_delete('series_weight')
    ->condition('tid', $form_state['values']['tid'])
    ->execute();
  foreach ($form_state['values']['nodes'] as $node) {
    db_insert('series_weight')
      ->fields(array(
        'tid' => $form_state['values']['tid'],
        'nid' => $node['nid'],
        'weight' => $node['weight'],
      ))
      ->execute();
  }

  db_update('taxonomy_term_data')
    ->fields(array(
      'description' => $form_state['values']['information']['description'],
    ))
    ->condition('tid', $form_state['values']['tid'])
    ->condition('vid', $form_state['values']['vid'])
    ->execute();

  // Store settings.
  $affected_rows = db_update('series_config')
    ->fields(array(
      'control_type' => $form_state['values']['control']['type'],
      'time_unit' => $form_state['values']['control']['time']['unit'],
      'time_step' => $form_state['values']['control']['time']['step'],
      // 'conclusion' => $form_state['values']['conclusion']['body'],
      // 'format' => $form_state['values']['conclusion']['format'],
    ))
    ->condition('tid', $form_state['values']['tid'])
    ->execute();
  if ($affected_rows <= 0) {
    db_insert('series_config')
      ->fields(array(
        'tid' => $form_state['values']['tid'],
        'control_type' => $form_state['values']['control']['type'],
        'time_unit' => $form_state['values']['control']['time']['unit'],
        'time_step' => $form_state['values']['control']['time']['step'],
        // 'conclusion' => $form_state['values']['conclusion']['body'],
        // 'format' => $form_state['values']['conclusion']['format'],
        'data' => NULL,
      ))
      ->execute();
  }

  // Clear the cache for this particular series.
  cache_clear_all('series:'. $form_state['values']['tid'], 'cache_series');
  cache_clear_all();
}

function series_create_form($form, $form_state, $series) {
  $vid = variable_get('series_vid', 0);
  $vocabulary = taxonomy_vocabulary_load($vid);
  $types = series_get_vocabulary_node_types($vocabulary);

  $form = array();
  if (!empty($types)) {
    $form['add_field']['#tree'] = TRUE;
    $form['add_field']['#theme'] = 'series_create_form';
    if (count($types) == 1) {
      $form['add_field']['select'] = array(
        '#type' => 'hidden',
        '#value' => key($types),
      );
      $form['add_field']['add'] = array(
        '#type' => 'button',
        '#value' => t('Add another'),
        '#validate' => array('series_edit_form_create_redirect'),
      );
    }
    else {
      $options = array(t('-- Select content type --'));
      foreach (node_get_types('names') as $type => $name) {
        if (isset($types[$type])) {
          $options[$type] = $name;
        }
      }
      $form['add_field']['select'] = array(
        '#type' => 'select',
        '#title' => t('Add another'),
        '#options' => $options,
      );
      $form['add_field']['add'] = array(
        '#type' => 'button',
        '#value' => t('Add another'),
        '#validate' => array('series_edit_form_create_redirect'),
        '#attributes' => array('class' => array('js-hide')),
      );
    }
  }
  return $form;
}

function series_edit_form_create_redirect($form, &$form_state) {
  if ($form_state['values']['add_field']['select']) {
    drupal_goto('node/add/'. $form_state['values']['add_field']['select'], array('series' => $form_state['values']['tid']));
  }
  else {
    form_set_error('add_field][select', t('Select a content type.'));
  }
}

function theme_series_edit_form($variables) {
  $form = $variables['form'];

  drupal_add_tabledrag('series-nodes', 'order', 'sibling', 'node-weight');

  $headers = array(t('Title'), t('Status'), t('Weight'), t('Operations'));
  $rows = array();

  foreach (element_children($form['nodes']) as $nid) {
    $form['nodes'][$nid]['weight']['#attributes']['class'] = array('node-weight');
    $operations = array(
      l(t('View'), 'node/'. $nid)
    );
    if (user_access('edit any '. $form['#series']->nodes[$nid]['type'] .' content')) {
      $operations[] = l(t('Edit'), 'node/'. $nid .'/edit');
    }
    $status = db_query("SELECT status FROM {node} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
    $row = array(
      array('data' => check_plain($form['#series']->nodes[$nid]['title'])),
      array('data' => ($status) ? t('Published') : t('Unpublished')),
      array('data' => drupal_render($form['nodes'][$nid])),
      array('data' => theme('item_list', array('items' => $operations, 'type' => 'ul', 'attributes' => array('class' => array('links'))))),
    );
    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }

  $output = drupal_render($form['information']);
  $output .= theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array('id' => 'series-nodes')));
  $output .= drupal_render($form['add_field']);
  $output .= drupal_render_children($form);
  return $output;
}

function theme_series_create_form($variables) {
  drupal_add_js(drupal_get_path('module', 'series') .'/js/admin.js');

  return '<div class="action">'. drupal_render_children($variables['form']) .'</div>';
}
